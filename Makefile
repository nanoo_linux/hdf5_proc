.PHONY=all clean

CXX_OPTS=-std=c++0x

all: main

main: main.o
	g++ $(CXX_OPTS) main.o -lhdf5 -Wall -o main

main.o: main.cpp build_vector_from_nxs.hpp build_and_save_image.hpp
	g++ $(CXX_OPTS) -c main.cpp -std=c++0x -O2 -Wall

clean:
	rm main.o main
