#pragma once

#include <hdf5.h>
#include <vector>

using std::vector;

template <typename T>
int do_type_depend_array_extract(hid_t, T *);

template <typename T=char>
int do_type_depend_array_extract(hid_t in, char *out)
{
  return ::H5Dread(in, H5T_NATIVE_CHAR, H5S_ALL, H5S_ALL, H5P_DEFAULT, out);
}

template <typename T=short>
int do_type_depend_array_extract(hid_t in, short *out)
{
  return ::H5Dread(in, H5T_NATIVE_SHORT, H5S_ALL, H5S_ALL, H5P_DEFAULT, out);
}

template <typename T=int>
int do_type_depend_array_extract(hid_t in, int *out)
{
  return ::H5Dread(in, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, out);
}

template <typename T=long long>
int do_type_depend_array_extract(hid_t in, long long *out)
{
  return H5Dread(in, H5T_NATIVE_LONG, H5S_ALL, H5S_ALL, H5P_DEFAULT, out);
}

template <typename T>
vector <T> build_vector_from_nxs
    (const char *filename,
    const char *table_name,
    const char *dataset_name)
{
  int ret;
  hid_t h_file;
  hid_t group;
  hid_t dataset;
  hid_t dataspace;
  hsize_t dims[2];
  vector <T> v;
  size_t array_size;
  T *p;

  h_file = ::H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
  if (h_file < 0) {
    goto err1;
  }

  group  = ::H5Gopen(h_file, table_name);
  if (group < 0) {
    goto err2;
  }

  dataset = ::H5Dopen(group, dataset_name);
  if (dataset < 0) {
    goto err3;
  }

  dataspace = ::H5Dget_space(dataset);
  if (dataspace < 0) {
    goto err4;
  }

  ret = ::H5Sget_simple_extent_dims(dataspace, dims, NULL);
  if (ret < 0) {
    goto err5;
  }

  array_size = (size_t)(dims[0]);
  p = new T[array_size];

  ret = do_type_depend_array_extract <T> (dataset, p);
  if (ret < 0) {
    goto err6;
  }

  v.assign(p, p+array_size);

  delete [] p;
  H5Sclose(dataspace);
  H5Dclose(dataset);
  H5Gclose(group);
  H5Fclose(h_file);

  return v;


  err6:
    delete [] p;
  err5:
    H5Sclose(dataspace);
  err4:
    H5Dclose(dataset);
  err3:
    H5Gclose(group);
  err2:
    H5Fclose(h_file);
  err1:
    throw int(-1);

}

