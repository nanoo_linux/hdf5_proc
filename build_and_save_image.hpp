#pragma once

#include <vector>
#include <utility>
#include <cstdio>

using std::vector;
using std::pair;

template <typename T>
int build_and_save_image2(const vector <pair <T, T> > &p)
{

  const unsigned XPAD = 9; //magic numbers. taken from detector hw info
  const unsigned YPAD = 16;

	vector <vector <unsigned> > image;
  for (unsigned i=0; i<XPAD; ++i) {
    vector <unsigned> v(YPAD, 0U);
    image.push_back(v);
  }

  unsigned min = 0;
  unsigned max = 0;
  auto it = p.begin();
  while (it != p.end()) {
    image[it->first][it->second] += 1U;
    if (min > image[it->first][it->second]) {
      min = image[it->first][it->second];
    }
    if (max < image[it->first][it->second]) {
      max = image[it->first][it->second];
    }
    ++it;
  }

	printf("P2\n%u %u\n255\n", XPAD, YPAD);

  auto it1 = image.begin();
  while (it1 != image.end()) {
    auto it2 = it1->begin();
    while (it2 != it1->end()) {
      printf("%4u ", (unsigned)(255.*(*it2-min)/(max-min)));
      ++it2;
    }
    ++it1;
  }

	return 0;
}

