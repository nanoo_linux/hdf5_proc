#include <cstdio>
#include "build_vector_from_nxs.hpp"
#include "build_and_save_image.hpp"


#include <map>
#include <vector>
#include <utility>

#include "config.hpp"

using std::vector;
using std::pair;


#ifdef T1
int main(int ac, char *av[])
{
	if (ac < 2) {
		fprintf(stderr, "nsx filename is required\n");
		return 0;
	}

  auto x_data = build_vector_from_nxs <char> (av[1], "flash", "dldXSpect");
  auto y_data = build_vector_from_nxs <char> (av[1], "flash", "dldYSpect");

  vector <pair <char, char> > v;
  auto ix = x_data.begin();
  auto iy = y_data.begin();
  while (ix != x_data.end() || iy != y_data.end()) {
    v.push_back(pair <char, char> (*ix, *iy));
    ++ix;
    ++iy;
  }

  return build_and_save_image2(v);

}
#endif

#ifdef T2
int main(int ac, char *av[])
{
	if (ac < 2) {
		fprintf(stderr, "nsx filename is required\n");
		return 0;
	}

  vector <short> spect_time = build_vector_from_nxs <short> (
		av[1], "flash", "dldTimeSpect");

//  vector <char> data_x = build_vector_from_nxs <char> (
//		av[1], "flash", "dldXSpect");

//  vector <char> data_y = build_vector_from_nxs <char> (
//		av[1], "flash", "dldYSpect");

  vector <char> pulser_data = build_vector_from_nxs <char> (
		av[1], "flash", "pulser");

  vector <long long> spect_index = build_vector_from_nxs <long long> (
		av[1], "flash", "dldSpectIndex");


	std::map <unsigned, unsigned> s_pos;
	std::map <unsigned, unsigned> s_neg;
	bool current_pulser_value;
	const size_t pulser_end = pulser_data.size();
	size_t i = 0;
	size_t j = 0;
	unsigned long long next_time_position_of_pulser_change = 0;
	while (i < pulser_end) {
		current_pulser_value = pulser_data[i] == 1? true: false;
		next_time_position_of_pulser_change = spect_index[i];
		while (j < next_time_position_of_pulser_change) {
			if (current_pulser_value) {
				s_pos[spect_time[j]] += 1;
			} else {
				s_neg[spect_time[j]] += 1;
			}
			++j;
		}
		++i;
	}


	FILE *out;
	out = fopen("out_pos", "w");
	if (!out) {
		fprintf(stderr, "Could not open file to save data");
		return 0;
	}
	{
		std::map <unsigned, unsigned> ::iterator it = s_pos.begin();
		while (it != s_pos.end()) {
			fprintf(out, "%u\t%u\n", it->first, it->second);
			++it;
		}
	}
	fclose(out);

	out = fopen("out_neg", "w");
	if (!out) {
		fprintf(stderr, "Could not open file to save data");
		return 0;
	}
	{
		std::map <unsigned, unsigned> ::iterator it = s_neg.begin();
		while (it != s_neg.end()) {
			fprintf(out, "%u\t%u\n", it->first, it->second);
			++it;
		}
	}
	fclose(out);
}

#endif
